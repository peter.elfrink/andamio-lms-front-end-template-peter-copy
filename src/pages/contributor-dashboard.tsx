import Head from "next/head";
import { Box, Center, Divider, Heading, Link as CLink, Text } from "@chakra-ui/react";
import { useContext } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import { hexToString } from "../utils";
import Link from "next/link";

// Add List of Mastery Assignments

export default function ContributorDashboardPage() {
  const ppblContext = useContext(PPBLContext);

  return (
    <>
      <Head>
        <title>Andamio LMS</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading>Student Page</Heading>
        <Text py="3">Connect your browser wallet to see some on-chain data that we have created in this course.</Text>
        <Divider />
        <Heading size="md" color="theme.yellow">
          Your Connected Contributor Token:
        </Heading>
        <pre>{JSON.stringify(ppblContext.connectedContribToken, null, 2)}</pre>
        <Heading size="md" color="theme.yellow">
          Your Student Reference Datum:
        </Heading>
        {ppblContext.contributorReferenceDatum ? (
          <>
            <Box p="3" bg="theme.green" color="theme.dark" my="3">
              <Text>Your lucky number is {ppblContext.contributorReferenceDatum?.fields[0].int}</Text>
            </Box>
            <Box p="3" bg="theme.green" color="theme.dark" my="3">
              <Text>Completed Modules:</Text>
              {ppblContext.contributorReferenceDatum?.fields[1].list.map((module: { bytes: string }, index: number) => (
                <Text key={index}>{hexToString(module.bytes)}</Text>
              ))}
            </Box>
          </>
        ) : (
          <Text>No Datum found. Please connect a browser wallet that holds a PPBL 2023 Token.</Text>
        )}
        <Divider mt="5" />
      </Box>
    </>
  );
}
