import {
  Box,
  Container,
  Divider,
  Flex,
  Heading,
  Image,
  Text,
  Stack,
  Center,
  Grid,
  GridItem,
  Button,
  useColorModeValue,
} from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const PBLFramework = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue")
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size={["lg", "2xl"]} color={textColorBlue} lineHeight="1.2em">
        The goal of any Project-Based Learning (PBL) Course is to support people to become Contributors to real
        Projects.
      </Heading>
    </Box>
  );
};

export default PBLFramework;
