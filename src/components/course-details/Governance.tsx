import { Button, Box, Divider, Heading, Text, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const Governance = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size="2xl" color={textColorBlue}>
        Contributor Governance
      </Heading>
      <Text marginTop="1em">
        You will participate in decision making by...
      </Text>

    </Box>
  );
};

export default Governance;
