import { Button, Box, Divider, Heading, ListItem, OrderedList, Text, Link as CLink, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import GetHelp from "../lms/Course/GetHelp";

const GettingHelp = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size="2xl" color={textColorBlue}>
       How to get help
      </Heading>

    </Box>
  );
};

export default GettingHelp;
